//Modul Path
//Modul Express
//Modul HBS View Engine
//Modul BodyParser 
//Modul MYSQL

const path = require('path');
const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();

//Connect to MySQL
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_product'
});

//Connect to DB
conn.connect((err) => {
    if(err) throw err;
    console.log('Database terhubung!');
});

//Set Views(Direktori Folder Views)
app.set('views', path.join(__dirname,'views'));

//Set View Engine
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

//Set folder public untuk file static
app.use('/assets', express.static(__dirname + '/public'));

//Route home
app.get('/',(req, res) => {
    let sql = "SELECT * FROM product";
    let query = conn.query(sql, (err, results) => {
        if(err) throw err;
        res.render('product_view',{
            results: results
        });
    });
});

//Route insert
app.post('/add',(req, res) => {
    let data = {
        product_name: req.body.product_name,
        product_price: req.body.product_price, 
    };
    let sql = "INSERT INTO product SET ?";
    let query = conn.query(sql, data,(err, results) => {
        if(err) throw err;
            res.redirect('/');
    });
});

//Route update
app.post('/update',(req, res) => {
    let sql = "UPDATE product SET product_name='"+req.body.product_name+"', product_price='"+req.body.product_price+"' WHERE product_id="+req.body.product_id_update;
    let query = conn.query(sql, (err, results) => {
        if(err) throw err;
            res.redirect('/');
    });
});

//Route delete
app.post('/delete', (req, res) => {
    let sql = "DELETE FROM product WHERE product_id="+req.body.product_id+"";
    let query = conn.query(sql, (err, results) => {
        if(err) throw err;
            res.redirect('/');
    });
});

//Server Node
app.listen(8000, () => {
    console.log('Server berjalan di http://localhost:8000');
});
